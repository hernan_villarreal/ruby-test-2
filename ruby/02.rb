def obtenerPrimos
  n_primos = []
  indice = 0
  for i in 1..1000
    contador = 0
    for j in 1..1000
      contador += 1 if i%j == 0
    end 
    if contador == 2
      n_primos[indice] = i 
      indice += 1
    end
  end
  n_primos
end