def contarLetra(cadena_original)
  #Cadena original
  cadena_original = cadena_original.downcase
  #Inicializar cadena a procesar
  cadena = ""
  #Caracteres permitidos
  caracteres = "abcdefghijklmnñopqrstuvwxyzáéíóúü"
  for i in 0..cadena_original.length-1
    for j in 0..caracteres.length-1
      cadena += cadena_original[i].to_s if cadena_original[i] == caracteres[j]
    end
  end
  #Para guardar la repetición más alta de una letra
  repeticion = 0
  #Hash para guardar la letra con su repetición
  letras = {}
  #Determinar las repeticiones de una letra
  for i in 0..cadena.length-1
    cantidad = 0
    for j in 0..cadena.length-1
      cantidad += 1 if cadena[i] == cadena[j]
    end    
    #Guardar las letras con sus repeticiones en el hash
    letras[cadena[i]] = cantidad
    #Guardar la repetición más alta de una letra
    repeticion = cantidad if cantidad >= repeticion
  end
  #Guardar las letras más repetidas para el mensaje
  cantidad = 0
  mensaje = ""
  letras.each do |letra, repeticiones|
    if repeticiones == repeticion
      cantidad = cantidad + 1
      if cantidad > 1 
        mensaje = mensaje + ", " + letra.upcase
      else 
        mensaje = letra.upcase
      end
    end
  end
  #Creación del mensaje
  if cantidad > 1 
    if repeticion > 1 
      mensaje = "Las letras que más se repiten son: " + mensaje + " con " + repeticion.to_s + " repeticiones."
    else 
      mensaje = "Las letras que más se repiten son: " + mensaje + " con " + repeticion.to_s + " repetición."
    end
  else
    if repeticion > 1 
      mensaje = "La letra que más se repite es la " + mensaje + " con " + repeticion.to_s + " repeticiones."
    else 
      mensaje = "La letra que más se repite es la " + mensaje + " con " + repeticion.to_s + " repetición."
    end
  end
  #Devolución del mensaje
  mensaje
end
