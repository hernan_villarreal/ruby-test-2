def verificaCapicua(cadena_original)
  #Filtro de caracteres
  caracteres_filtro = "0123456789.- "
  #Inicialización de variables
  cadena_filtrada = ""
  cadena_al_reves = ""
  #Procesar filtro
  for i in 0..cadena_original.length-1
    #Flag - Auxiliar
    flag = true
    for j in 0..caracteres_filtro.length-1
      flag = false if cadena_original[i] == caracteres_filtro[j] if flag
    end
    cadena_filtrada += cadena_original[i] if flag 
  end  
  #Revertir cadena
  cadena_al_derecho = cadena_filtrada
  i = cadena_al_derecho.length
  until i < 0 
    cadena_al_reves += cadena_al_derecho[i].to_s
    i -= 1
  end
  #Comparar cadenas para verificar capicúa
  if cadena_al_derecho.downcase == cadena_al_reves.downcase 
    true
  else 
    false
  end
end